//
// Created by ricar on 16.12.2022.
//

#include"bmp.h"

#ifndef IMAGE_ROTATION_ROTATION_H
#define IMAGE_ROTATION_ROTATION_H

// This function rotates an image by 90 degrees clockwise.
// The input "source" is the image to be rotated.
struct image rotation(struct image source);

#endif //IMAGE_ROTATION_ROTATION_H

