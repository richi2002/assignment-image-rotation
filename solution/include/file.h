//
// Created by ricar on 16.12.2022.
//

#ifndef IMAGE_ROTATION_FILE_H
#define IMAGE_ROTATION_FILE_H

#include <stdbool.h>
#include <stdio.h>

enum FileMode { READ, WRITE, APPEND };

// This function attempts to open a file with the given name and mode.
bool check_file_opened(FILE **file, const char *name, enum FileMode mode);

// This function closes a file that was previously opened with check_file_opened().
bool check_file_closed(FILE **file);

#endif //IMAGE_ROTATION_FILE_H



