//
// Created by ricar on 16.12.2022.
//

#ifndef IMAGE_ROTATION_IMAGE_H
#define IMAGE_ROTATION_IMAGE_H

#include<malloc.h>
#include<stdint.h>
#include<stdio.h>

// This directive tells the compiler to pack the structure members
// tightly together, without adding any padding bytes between them
#pragma pack(push,1)

// This structure represents an image, with a width and height
struct image{
    uint64_t width;
    uint64_t height;
    struct pixel* data;
};

// This structure represents a pixel, with 8-bit red, green, and blue values.

struct pixel{
    __attribute__((unused)) 
    uint8_t b; 
    uint8_t g;
    uint8_t r;
};

// This directive tells the compiler to revert to its default packing of structure members
#pragma pack(pop)

// This function configures an image structure with the given width and height.
//void configure_image(struct image *const img, uint32_t width, uint32_t height);
struct image *configure_image(struct image const *img, uint32_t width, uint32_t height) ;

// This function frees the memory allocated for the image data in the given image structure.
void free_img (struct image const *img );

#endif



