//
// Created by ricar on 16.12.2022.
//

#ifndef IMAGE_ROTATION_BMP_H
#define IMAGE_ROTATION_BMP_H

#include<malloc.h>
#include<stdint.h>
#include<stdio.h>

#include "image.h"

enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_ERROR
    /* code for other errors  */
};

// This function reads a BMP file from the given input buffer and stores the image data in the given image structure.
enum read_status read_bmp(FILE *in, struct image const *img );

/*  serializer   */
enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
    /* code for other errors  */
};

// This function writes an image to a BMP file in the given output buffer.
enum write_status to_bmp(const char* out, struct image const* img );


#endif //IMAGE_ROTATION_BMP_H
