//
// Created by ricar on 16.12.2022.
//

#include "bmp.h"
#include "file.h"
#include "image.h"
#include "rotation.h"

    int main(int argc, char** argv) {
    // Check if there are enough arguments
    if (argc != 3) {
        return 1;
    }

    // Get the input and output filenames from the command line arguments
    const char *input_name = argv[1];
    const char *output_name = argv[2];

    // Initialize the image structure
    struct image image = {0};

    // Open the input file
    FILE *in = fopen(input_name, "rb");
    if (in == NULL) {
        return 1;
    }

    // Read the input file and check for errors
    enum read_status read_result = read_bmp(in, &image);
    if (read_result != READ_OK) {
        free_img(&image);
        fclose(in);
        return 1;
    }

    // Close the input file
    fclose(in);

    // Rotate the image and check for errors
    struct image rotated_image = rotation(image);
    int write_result = to_bmp(output_name, &rotated_image);
    if (write_result != WRITE_OK) {
        free_img(&image);
        free_img(&rotated_image);
        return 1;
    }

    // Free the image data
    free_img(&image);
    free_img(&rotated_image);

    // Return success
    return 0;
}




