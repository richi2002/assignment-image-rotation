//
// Created by ricar on 16.12.2022.
//

#include "image.h"
#include <inttypes.h>
#include <malloc.h>
#include <stdlib.h>

// configure_image sets the width, height, and data fields of the input image struct
//void configure_image(struct image *const img,uint32_t width, uint32_t height){
struct image * configure_image(const struct image *img, uint32_t width, uint32_t height) {
    struct image *new_img = malloc(sizeof(struct image));
    new_img->width = width;
    new_img->height = height;
    new_img->data = malloc(sizeof (struct pixel) * width * height);
    for (uint32_t i = 0; i < img->width * img->height; i++) {
        new_img->data[i] = img->data[i];
    }
    return new_img;
}


void free_img (const struct image *img ){
    // free the memory allocated for the image data array
    free((void *) img->data);
}
