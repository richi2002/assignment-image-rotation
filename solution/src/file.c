//
// Created by ricar on 16.12.2022.
//

#include "file.h"


bool check_file_opened(FILE** file, const char* filename, enum FileMode mode) {
    const char* modeString;
    switch (mode) {
        case READ:
            modeString = "rb";
            break;
        case WRITE:
            modeString = "wb";
            break;
        case APPEND:
            modeString = "a";
            break;
        default:
            fprintf(stderr, "Invalid file mode\n");
            return false;
    }
    *file = fopen(filename, modeString);
    if (!*file) {
        fprintf(stderr, "Error opening file\n");
        return false;
    }
    return true;
}


// check_file_closed closes a file and returns a boolean indicating success or failure
bool check_file_closed(FILE **file){
    // if the file pointer is null, return false
    if(!*file){
        return false;
    }
    // close the file and return true if the operation was successful, or false otherwise
    return (fclose(*file) == 0);
}
