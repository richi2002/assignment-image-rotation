//
// Created by ricar on 16.12.2022.
//

#include "image.h"
#include "rotation.h"

// location_PIXEL returns a pointer to the pixel at the specified location in the image
struct pixel* location_PIXEL(struct image src, uint32_t h, uint32_t w){
    // calculate the location of the pixel in the image data array and return a pointer to it
    return src.data + src.width*h + w;
}

// rotation rotates the input image by 90 degrees clockwise
struct image rotation(struct image const source){

    // create a new image to store the rotated result
    struct image result = {0};
    // configure the result image with the rotated dimensions of the source image
    configure_image(&result,source.height,source.width);

    // loop through the result image pixels
    for (uint32_t i = 0; i < result.height; i++) {
        for (uint32_t j = 0; j < result.width; j++) {
            // set the result pixel to the source pixel at the corresponding rotated location
            *(location_PIXEL(result,i,j)) = *(location_PIXEL(source,result.width-j-1,i));
        }
    }
    // return the rotated result image
    return result;
}


